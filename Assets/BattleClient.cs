using System;
using System.IO;
using System.IO.Compression;
using System.Net;
using System.Net.Sockets;
using System.Text;
using UnityEngine;
using LiteNetLib;
using LiteNetLib.Utils;
using Newtonsoft.Json;
using RealmManagerClientRequests;
using Scripts.Utils;
using SharedBaseNetworking.Packets;

public class BattleClient : MonoBehaviour, INetEventListener
{
    private NetManager _netManager;
    private NetPeer server;
    private static readonly NetPacketProcessor _netPacketProcessor = new NetPacketProcessor();
    private PacketProcessor packetProcessor;
    private IRealmManagerClient _realmManagerClient;
    private void Awake()
    {
        JsonConvert.DefaultSettings = () => new JsonSerializerSettings() {TypeNameHandling = TypeNameHandling.All};
        _realmManagerClient = new RealmManagerClient(new ProxyRealmManager(this));
        packetProcessor = new PacketProcessor(new MessageParser(_realmManagerClient));
        _netManager = new NetManager(this)
        {
            DisconnectTimeout = 99999, SimulateLatency = false, SimulationMinLatency = 100, SimulationMaxLatency = 300,
            SimulatePacketLoss = false, SimulationPacketLossChance = 10
        };
        _netPacketProcessor.SubscribeReusable<ExamplePacket, NetPeer>(OnExamplePacketReceived);
    }

    public IRealmManagerClient GetRMClient()
    {
        return _realmManagerClient;
    }
    
    public static byte[] Decompress(byte[] data)
    {
        using (var compressedStream = new MemoryStream(data))
        using (var zipStream = new GZipStream(compressedStream, CompressionMode.Decompress))
        using (var resultStream = new MemoryStream())
        {
            zipStream.CopyTo(resultStream);
            return resultStream.ToArray();
        }
    }

    byte[] Compress(byte[] data)
    {
        using (var compressedStream = new MemoryStream())
        using (var zipStream = new GZipStream(compressedStream, CompressionMode.Compress))
        {
            zipStream.Write(data, 0, data.Length);
            zipStream.Close();
            return compressedStream.ToArray();
        }
    }
    
    public void SendReliable(object data)
    {
        var packet = new ExamplePacket();
        var msg = JsonConvert.SerializeObject(data);
        packet.Data = Compress(Encoding.Unicode.GetBytes(msg));
        packet.DataSize = packet.Data.Length;
        packet.Id = Guid.NewGuid().ToByteArray();
        packet.PacketsCount = 1;
        packet.Token = new byte[]{1};
        _netPacketProcessor.Send(server, packet, DeliveryMethod.ReliableOrdered);
    }
    
    private void OnExamplePacketReceived(ExamplePacket packet, NetPeer peer)
    {
        //Debug.Log(new Guid(packet.Id));
        packetProcessor.ProcessPacket(packet);
    }

    private void Start()
    {
        _netManager.Start();
        _netManager.UpdateTime = 15;
        server = _netManager.Connect("192.168.0.110", 7777, "123");
    }

    private void Update()
    {
        _netManager.PollEvents();
    }

    private void OnDestroy()
    {
        _netManager?.DisconnectAll();
        _netManager?.Stop();
    }

    public void OnPeerConnected(NetPeer peer)
    {
        Debug.Log("[CLIENT] We connected to " + peer.EndPoint);
    }

    public void OnNetworkError(IPEndPoint endPoint, SocketError socketErrorCode)
    {
        Debug.Log("[CLIENT] We received error " + socketErrorCode);
    }

    public void OnNetworkReceive(NetPeer peer, NetPacketReader reader, DeliveryMethod deliveryMethod)
    {
        _netPacketProcessor.ReadPacket(reader);
    }

    public void OnNetworkReceiveUnconnected(IPEndPoint remoteEndPoint, NetPacketReader reader,
        UnconnectedMessageType messageType)
    {
    }

    public void OnNetworkLatencyUpdate(NetPeer peer, int latency)
    {
    }

    public void OnConnectionRequest(ConnectionRequest request)
    {
    }

    public void OnPeerDisconnected(NetPeer peer, DisconnectInfo disconnectInfo)
    {
        Debug.Log("[CLIENT] We disconnected because " + disconnectInfo.Reason);
    }
    
}