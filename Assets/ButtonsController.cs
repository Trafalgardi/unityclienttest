﻿using System;
using System.Collections;
using System.Collections.Generic;
using BattleRequests;
using Scripts.Utils;
using UnityEngine;

public class ButtonsController : MonoBehaviour
{
    private IBattleController _battleController;
    public BattleClient BattleClient;
    private void Awake()
    {
        _battleController = new ProxyBattleController(BattleClient);
    }

    public void OnButton1Click()
    {
        _battleController.AnnouncePlayerAbilityWish(BattleClient.GetRMClient().CharacterId, BattleClient.GetRMClient().CharacterId, 1);
    }
    
    public void OnButton2Click()
    {
        _battleController.AnnouncePlayerAbilityWish(BattleClient.GetRMClient().CharacterId, BattleClient.GetRMClient().CharacterId, 2);
    }
}
