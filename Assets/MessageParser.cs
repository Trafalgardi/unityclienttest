﻿﻿using System;
 using ClientBattleAPI;
 using Newtonsoft.Json;
 using RealmManagerClientRequests;
 using RealmManagerClientRequests.Interfaces;
 using UnityEngine;

public class MessageParser
{
    private IRealmManagerClient _realmManagerClient;
    public MessageParser(IRealmManagerClient realmManagerClient)
    {
        _realmManagerClient = realmManagerClient;
    }

    public void Parse(string msg)
    {
        if (string.IsNullOrEmpty(msg))
            return;
        object obj = null;
        try
        {
            
            obj = JsonConvert.DeserializeObject(msg,
                new JsonSerializerSettings() {TypeNameHandling = TypeNameHandling.All, Formatting = Formatting.Indented, });
        }
        catch (Exception e)
        {
            obj = null;
            Debug.LogError(e);
            Debug.LogError(msg);
        }
        finally
        {
            if (obj is Notify notify)
            {
                foreach (var gameEvent in notify.eventListInfo.EventList)
                {
                    if(gameEvent.Message.Contains("kyri_1"))
                        Debug.LogError($"{gameEvent.Time} : {gameEvent.Message}");
                    else
                        Debug.Log($"{gameEvent.Time} : {gameEvent.Message}");
                }
            }

            if (obj is IRealmManagerToClientRequest request)
            {
                request.Apply(_realmManagerClient);
            }
            
        }
        
    }
}