﻿﻿using System;
using System.Collections.Generic;
using System.Linq;
 using SharedBaseNetworking.Packets;

 namespace Scripts.Utils
{
    public class PacketAssembler
    {
        public ExamplePacket[] packets;
        private Action<ExamplePacket[], Guid> assembledCallback;
        public Guid id;
        private int totalPackets;
        private int GCScore;
        private const int MAX_GC_SCORE = 60;
        public PacketAssembler(int count, Guid id, Action<ExamplePacket[], Guid> assembledCallback)
        {
            packets = new ExamplePacket[count];
            this.id = id;
            this.assembledCallback = assembledCallback;
        }

        public void Init(Guid id, int totalPackets)
        {
            
            if (totalPackets > packets.Length)
            {
                throw new ArgumentException("PacketAssembler.Init() cannot handle more packets than packets array length");
            }
            this.id = id;
            this.totalPackets = totalPackets;
        }

        public void Clear()
        {
            GCScore = 0;
            for (int i = 0; i < packets.Length; i++)
            {
                packets[i] = null;
            }
            id = Guid.Empty;
        }
        
        public void AddPacket(ExamplePacket packet, Stack<ExamplePacket> pool)
        {
            int index = packet.PacketNumber;
            if(index > totalPackets)
                return;
            if (packets[index] != null)
            {
                packet.Id = Guid.Empty.ToByteArray();
                pool.Push(packet);
                return;
            }
            packets[index] = packet;
            CheckComplete();
        }

        public bool GarbageCollection()
        {
            GCScore++;
            return GCScore > MAX_GC_SCORE;
        }
        
        private void CheckComplete()
        {
            for (int i = 0; i < totalPackets; i++)
            {
                if(packets[i] == null)
                    return;
            }

            var result = packets.Where(packet => packet != null).ToArray();
            assembledCallback?.Invoke(result, id);
        }
    }
}