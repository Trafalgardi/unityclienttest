﻿﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
 using SharedBaseNetworking.Packets;
 using UnityEngine;

namespace Scripts.Utils
{
    public class PacketProcessor
    {
        private Dictionary<Guid, PacketAssembler> assemblers;
        private Stack<ExamplePacket> PacketPool;
        private Stack<PacketAssembler> AssemblerPool;
        private const int PACKET_POOL_SIZE = 200;
        private const int PACKET_DATA_SIZE = 500;
        private const int PACKET_ID_SIZE = 16;

        private const int ASSEMBLERS_POOL_SIZE = 100;
        private const int ASSEMBLERS_PACKETS_COUNT = 10;
        private MessageParser messageParser;

        public PacketProcessor(MessageParser parser)
        {
            messageParser = parser;
            assemblers = new Dictionary<Guid, PacketAssembler>();
            PacketPool = new Stack<ExamplePacket>();
            for (int i = 0; i < PACKET_POOL_SIZE; i++)
            {
                var packet = new ExamplePacket {Data = new byte[PACKET_DATA_SIZE], Id = new byte[PACKET_ID_SIZE]};
                PacketPool.Push(packet);
            }

            AssemblerPool = new Stack<PacketAssembler>();
            for (int i = 0; i < ASSEMBLERS_POOL_SIZE; i++)
            {
                var assembler = new PacketAssembler(ASSEMBLERS_PACKETS_COUNT, Guid.Empty, PacketAssembled);
                AssemblerPool.Push(assembler);
            }
            assemblersToRecycle = new List<PacketAssembler>();
        }

        private List<PacketAssembler> assemblersToRecycle;
        public void ProcessPacket(ExamplePacket sourcePacket)
        {
            if (PacketPool.Count == 0)
            {
                Debug.LogError("PacketPool is empty!");
                return;
            }
            
            var poolPacket = GetPacketFromPool(sourcePacket);
            var id = new Guid(poolPacket.Id);

             foreach (var packetAssembler in assemblers)
             {
                 if (packetAssembler.Value.GarbageCollection())
                 {
                     assemblersToRecycle.Add(packetAssembler.Value);
                 }
             }

             foreach (var packetAssembler in assemblersToRecycle)
             {
                 RecycleAssembler(packetAssembler.packets, packetAssembler.id);
             }
             assemblersToRecycle.Clear();
             

            if (!assemblers.ContainsKey(id))
            {
                if (AssemblerPool.Count == 0)
                {
                    Debug.LogError("AssemblerPool is empty!");
                    return;
                }

                var assembler = GetAssemblerFromPool(poolPacket.PacketsCount, id);
                assemblers.Add(id, assembler);
            }

            assemblers[id].AddPacket(poolPacket, PacketPool);
            //Debug.Log($"ID: {id}, Number {packet.PacketNumber}, Total packets: {packet.PacketsCount}");
        }

        private ExamplePacket GetPacketFromPool(ExamplePacket sourcePacket)
        {
            var poolPacket = PacketPool.Pop();

            sourcePacket.Data.CopyTo(poolPacket.Data, 0);
            sourcePacket.Id.CopyTo(poolPacket.Id, 0);

            poolPacket.DataSize = sourcePacket.DataSize;
            poolPacket.PacketNumber = sourcePacket.PacketNumber;
            poolPacket.PacketsCount = sourcePacket.PacketsCount;

            return poolPacket;
        }

        private PacketAssembler GetAssemblerFromPool(int count, Guid id)
        {
            var assembler = AssemblerPool.Pop();
            assembler.Init(id, count);
            return assembler;
        }

        private void PacketAssembled(ExamplePacket[] packets, Guid assemblerId)
        {
            var data = MergeData(packets);
            var str = Encoding.Unicode.GetString(BattleClient.Decompress(data));
            messageParser.Parse(str);
            
            RecycleAssembler(packets, assemblerId);

        }
        
        
        
        private void RecycleAssembler(ExamplePacket[] packets, Guid assemblerId)
        {
            foreach (var packet in packets)
            {
                if (packet != null)
                {
                    PacketPool.Push(packet);
                }
            }
                
            
            var assembler = assemblers[assemblerId];
            assemblers.Remove(assemblerId);
            AssemblerPool.Push(assembler);
            assembler.Clear();
        }

        private byte[] MergeData(ExamplePacket[] packets)
        {
            var size = packets.Where(packet => packet != null).Aggregate(0, (current, packet) => current + packet.DataSize);

            var result = new byte[size];
            int i = 0;
            foreach (var packet in packets)
            {
                if(packet == null)
                    continue;
                for (int j = 0; j < packet.DataSize; j++)
                {
                    result[i] = packet.Data[j];
                    ++i;
                }
            }
            
            return result;
        }
    }
}