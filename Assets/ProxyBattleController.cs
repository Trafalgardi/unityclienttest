﻿using System;
using BattleRequests;
using RealmManagerRequests;

namespace Scripts.Utils
{
    public class ProxyBattleController : IBattleController
    {
        private BattleClient _battleClient;

        public ProxyBattleController(BattleClient battleClient)
        {
            _battleClient = battleClient;
        }

        public void AnnouncePlayerBasicAttackTarget(Guid subject, Guid targetId)
        {
            throw new NotImplementedException();
        }

        public void AnnouncePlayerAbilityWish(Guid subject, Guid targetId, int abilityIndex)
        {
            var battleRequest = new AbilityRequest()
                {AbilityIndex = abilityIndex, SubjectID = subject, TargetID = targetId};
            var realmManagerRequest = new BattleRequestWrapper(battleRequest, _battleClient.GetRMClient().ClientId);
            _battleClient.SendReliable(realmManagerRequest);
        }
    }
}