﻿using System;
using BattleRequests;
using RealmManagerRequests;

namespace Scripts.Utils
{
    public class ProxyRealmManager : IRealmController
    {
        private BattleClient _battleClient;

        public ProxyRealmManager(BattleClient battleClient)
        {
            _battleClient = battleClient;
        }

        public void CreatePersonalRealm()
        {
            throw new NotImplementedException();
        }

        public void CreateBattleRealm(Guid requesterId)
        {
            _battleClient.SendReliable(new CreateBattleRealmRequest(){RequesterId = requesterId});
        }

        public void ReceiveBattleRequest(IBattleRequest battleRequest, Guid requesterId)
        {
            throw new NotImplementedException();
        }
    }
}