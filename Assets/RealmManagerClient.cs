﻿using System;
using RealmManagerClientRequests;
using RealmManagerRequests;
using UnityEngine;

namespace Scripts.Utils
{
    public class RealmManagerClient : IRealmManagerClient
    {
        private IRealmController _realmController;


        public Guid ClientId { get; private set; }
        public Guid CharacterId { get; private set; }

        public RealmManagerClient(IRealmController realmController)
        {
            _realmController = realmController;
        }
        public void ReceiveCharacterId(Guid id)
        {
            CharacterId = id;
        }
        public void ReceiveId(Guid id)
        {
            ClientId = id;
            Debug.Log("CreateBattleRealm");
            _realmController.CreateBattleRealm(id);
        }
    }
}